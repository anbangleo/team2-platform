package org.xploration.team2.platform;

import jade.core.Agent;
import jade.core.AID;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import jade.content.lang.Codec;
import jade.content.lang.Codec.*;
import jade.content.lang.sl.*;
import jade.content.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;

import java.util.*;

import org.xploration.ontology.*;
import org.xploration.ontology.impl.*;
import org.xploration.team2.Constants;

public class RegistrationDesk extends Agent {
    
    private List<AID> registrationList = new ArrayList<AID>();
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();

    protected void setup() {
        System.out.println(getLocalName()+": has entered into the system");
        //Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        System.out.println(getLocalName()+": codec: " + codec.getName());
        System.out.println(getLocalName()+": ontology: " + ontology.getName());
        try {
            // Creates its own description
            DFAgentDescription dfd = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setName(this.getName());
            sd.setType(Constants.REGISTRATION_DESK_SERVICE_NAME);
            dfd.addServices(sd);
            // Registers its description in the DF
            DFService.register(this, dfd);
            System.out.println(getLocalName()+": registered in the DF");
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        
        addBehaviour(getRegistrationListenerBehaviour());
    }
    
    private Behaviour getRegistrationListenerBehaviour() {
        return new CyclicBehaviour(this) {

            @Override
            public void action() {
                // Waits for estimation requests
                ACLMessage msg = blockingReceive(
                    MessageTemplate.and(
                        MessageTemplate.MatchLanguage(codec.getName()), 
                        MessageTemplate.and(
                            MessageTemplate.MatchOntology(ontology.getName()),
                            MessageTemplate.MatchPerformative(ACLMessage.REQUEST)
                        )
                    )
                );
                if (msg != null) {
                    // If an REGISTRATION request arrives (type REQUEST)
                    // it answers with the REFUSE, AGREE or NU
                    
                    // The ContentManager transforms the message content (string)
                    // in java objects
                    ContentElement ce;
                    try {
                        ce = getContentManager().extractContent(msg);
                    
                        // We expect an action inside the message
                        if (ce instanceof Action) {
                            Action agAction = (Action) ce;
                            Concept conc = agAction.getAction();
                            // If the action is RegistrationRequest...
                            if (conc instanceof RegistrationRequest) {
                                AID fromAgent = msg.getSender();
                                System.out.println(myAgent.getLocalName()+": received registration request from "+(msg.getSender()).getLocalName());
                                Team requestorTeam = ((RegistrationRequest) conc).getTeam();
                                System.out.println(myAgent.getLocalName()+": registration request for "+requestorTeam);
                                
                                ACLMessage reply = msg.createReply();
    
                                // An ack of registration request is sent
                                reply.setLanguage(codec.getName());
                                reply.setOntology(ontology.getName());
                                // A PROPOSE message is sent
                                reply.setPerformative(ACLMessage.AGREE);
                                
                                // The ContentManager transforms the java objects in strings
                                myAgent.send(reply);
                                System.out.println(myAgent.getLocalName()+": reg req ack sent");
                                
                                // Asks the estimation to the painter
                                ACLMessage finalMsg = new ACLMessage(ACLMessage.INFORM);
                                finalMsg.addReceiver(fromAgent);
                                finalMsg.setLanguage(codec.getName());
                                finalMsg.setOntology(ontology.getName());
                                myAgent.send(finalMsg);
                                System.out.println(myAgent.getLocalName()+": reg resp sent");
                            }
                        }
                    } catch (CodecException | OntologyException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        
                        AID fromAgent = msg.getSender();
                        ACLMessage reply = msg.createReply();
                        reply.setLanguage(codec.getName());
                        reply.setOntology(ontology.getName());
                        reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                        myAgent.send(reply);
                        System.out.println(myAgent.getLocalName()+": NOT UNDERSTOOD sent");
                    }
                }
            }
            
        };
    }

    protected void takeDown() {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        }
        catch (FIPAException fe) {
            fe.printStackTrace();
        }
        
        // Printout a dismissal message
        System.out.println("RegistrationDesk agent "+getAID().getName()+" terminating.");
    }

}
